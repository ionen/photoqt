<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt-BR" sourcelanguage="en">
  <context>
    <name>MainMenu</name>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="309"/>
      <source>Settings</source>
      <translation>Configurações</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="233"/>
      <source>Slideshow</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Apresentação de slides</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="86"/>
      <source>Rename file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Rename file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="91"/>
      <source>Copy file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Copy file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="96"/>
      <source>Move file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Move file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="101"/>
      <source>Delete file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Delete file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="114"/>
      <source>Tag faces</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Tag faces</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="132"/>
      <source>Hide metadata</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Hide metadata</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="132"/>
      <source>Show metadata</source>
      <translation type="unfinished">Show metadata</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="137"/>
      <source>Hide histogram</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Hide histogram</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="137"/>
      <source>Show histogram</source>
      <translation type="unfinished">Show histogram</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="124"/>
      <source>Set as wallpaper</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Set as wallpaper</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="323"/>
      <source>Quit</source>
      <translation>Sair</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="119"/>
      <source>Scale image</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Scale image</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="173"/>
      <source>Navigation</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Navigation</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="177"/>
      <source>Browse images</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Browse images</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="181"/>
      <source>first</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Primeiro</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="184"/>
      <source>last</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Último</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="193"/>
      <source>Zoom</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Zoom</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="197"/>
      <source>Actual size</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Actual size</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="209"/>
      <source>Rotation/Flip</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Rotation/Flip</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="213"/>
      <source>Horizontal flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Horizontal flip</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="217"/>
      <source>Vertical flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Vertical flip</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="221"/>
      <source>Reset flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Reset flip</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="225"/>
      <source>Reset rotation</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Reset rotation</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="237"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="250"/>
      <source>Start</source>
      <extracomment>This is an entry in the main menu on the right, used as in &quot;START slideshow/sorting&quot;. Please keep short!</extracomment>
      <translation type="unfinished">Start</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="241"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="254"/>
      <source>Setup</source>
      <extracomment>This is an entry in the main menu on the right, used as in &quot;SETUP slideshow/sorting&quot;. Please keep short!</extracomment>
      <translation type="unfinished">Setup</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="246"/>
      <source>Advanced Sort</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Advanced Sort</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="259"/>
      <source>Other</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Other</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="262"/>
      <source>Filter images</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Filter images</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="277"/>
      <source>External</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">External</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="312"/>
      <source>About</source>
      <translation type="unfinished">About</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="320"/>
      <source>Online help</source>
      <translation type="unfinished">Online help</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="124"/>
      <source>Click and drag to resize main menu</source>
      <translation>Clique e arraste para redimensionar o menu principal</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="201"/>
      <source>Reset zoom</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Reset zoom</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="266"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Streaming (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="109"/>
      <source>Copy to clipboard</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation type="unfinished">Copy to clipboard</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="270"/>
      <source>Open in default file manager</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Abrir no gerenciador de arquivos padrão</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenuPopout.qml" line="34"/>
      <source>Main Menu</source>
      <extracomment>Window title</extracomment>
      <translation>Menu principal</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenuGroup.qml" line="100"/>
      <location filename="../qml/menumeta/PQMainMenuGroup.qml" line="205"/>
      <source>You need to load an image first.</source>
      <translation type="unfinished">You need to load an image first.</translation>
    </message>
  </context>
  <context>
    <name>PQImageFormats</name>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="48"/>
      <location filename="../cplusplus/settings/imageformats.cpp" line="60"/>
      <source>ERROR getting default image formats</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ERRO! Obtendo formato de imagens padrão</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="49"/>
      <source>I tried hard, but I just cannot open even a read-only version of the database of default image formats.</source>
      <translation>I tentei arduamente, porém, eu simplesmente não posso mesmo abrir uma versão somente leitura of banco de dados dos formatos de imagem padrão.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="49"/>
      <location filename="../cplusplus/settings/imageformats.cpp" line="61"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Alguma deu terrivelmente errado em algum lugar!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="61"/>
      <source>I tried hard, but I just cannot open the database of default image formats.</source>
      <translation>I tentei arduamente, mas, i simplesmente não posso abrir o banco de dados dos formatos de imagem padrão.</translation>
    </message>
  </context>
  <context>
    <name>PQMetaData</name>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="421"/>
      <source>yes</source>
      <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
      <translation>Sim</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="423"/>
      <source>no</source>
      <extracomment>This string identifies that flash was not fired, stored in image metadata</extracomment>
      <translation>Não</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="425"/>
      <source>No flash function</source>
      <extracomment>This string refers to the absense of a flash, stored in image metadata</extracomment>
      <translation>Sem função flash</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="427"/>
      <source>strobe return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Luz de retorno estroboscópica não detectada</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="429"/>
      <source>strobe return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luz de retorno estroboscópia detectada</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="431"/>
      <source>compulsory flash mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>modo de flash obrigatório</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="433"/>
      <source>auto mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>moto automático</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="435"/>
      <source>red-eye reduction mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>modo de redução de olho vermelho</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="437"/>
      <source>return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luz de retorno detectada</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="439"/>
      <source>return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luz de returno não detectada</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="487"/>
      <source>Invalid flash</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>flash inválido</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="498"/>
      <source>Standard</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Padrão</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="501"/>
      <source>Landscape</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Paisagem</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="504"/>
      <source>Portrait</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Retrato</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="507"/>
      <source>Night Scene</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Cena noturna</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="510"/>
      <source>Invalid Scene Type</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Tipo de cena inválida</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="563"/>
      <source>Unknown</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Desconhecido</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="566"/>
      <source>Daylight</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Luz do dia</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="569"/>
      <source>Fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Flurescente</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="572"/>
      <source>Tungsten (incandescent light)</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Tungstênio (luz incandescente)</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="575"/>
      <source>Flash</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Flash</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="578"/>
      <source>Fine weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Clima bom</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="581"/>
      <source>Cloudy Weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Clima nublado</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="584"/>
      <source>Shade</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Sombra</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="587"/>
      <source>Daylight fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Luz do dia fluorescente</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="590"/>
      <source>Day white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Branco dia fluorescente</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="593"/>
      <source>Cool white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Branco frio fluorescente</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="596"/>
      <source>White fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Branco fluorescente</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="599"/>
      <location filename="../cplusplus/scripts/metadata.cpp" line="602"/>
      <location filename="../cplusplus/scripts/metadata.cpp" line="605"/>
      <source>Standard light</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Luz padrão</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="618"/>
      <source>Other light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Outra fonte de luz</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="621"/>
      <source>Invalid light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fonte de luz inválida</translation>
    </message>
  </context>
  <context>
    <name>PQPrintSupport</name>
    <message>
      <location filename="../cplusplus/print/printsupport.cpp" line="52"/>
      <source>Print</source>
      <translation type="unfinished">Print</translation>
    </message>
  </context>
  <context>
    <name>PQSettings</name>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="64"/>
      <source>ERROR getting database with default settings</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation type="unfinished">ERROR getting database with default settings</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="65"/>
      <source>I tried hard, but I just cannot open even a read-only version of the settings database.</source>
      <translation type="unfinished">I tried hard, but I just cannot open even a read-only version of the settings database.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="65"/>
      <location filename="../cplusplus/settings/settings.cpp" line="77"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation type="unfinished">Something went terribly wrong somewhere!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="76"/>
      <source>ERROR opening database with default settings</source>
      <translation type="unfinished">ERROR opening database with default settings</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="77"/>
      <source>I tried hard, but I just cannot open the database of default settings.</source>
      <translation type="unfinished">I tried hard, but I just cannot open the database of default settings.</translation>
    </message>
  </context>
  <context>
    <name>PQShortcuts</name>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="48"/>
      <source>ERROR getting database with default shortcuts</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation type="unfinished">ERROR getting database with default shortcuts</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="49"/>
      <source>I tried hard, but I just cannot open even a read-only version of the shortcuts database.</source>
      <translation type="unfinished">I tried hard, but I just cannot open even a read-only version of the shortcuts database.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="49"/>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="61"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation type="unfinished">Something went terribly wrong somewhere!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="60"/>
      <source>ERROR opening database with default settings</source>
      <translation type="unfinished">ERROR opening database with default settings</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="61"/>
      <source>I tried hard, but I just cannot open the database of default shortcuts.</source>
      <translation type="unfinished">I tried hard, but I just cannot open the database of default shortcuts.</translation>
    </message>
  </context>
  <context>
    <name>PQStartup</name>
    <message>
      <location filename="../cplusplus/startup/startup.cpp" line="56"/>
      <source>SQLite error</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation type="unfinished">SQLite error</translation>
    </message>
    <message>
      <location filename="../cplusplus/startup/startup.cpp" line="57"/>
      <source>You seem to be missing the SQLite driver for Qt. This is needed though for a few different things, like reading and writing the settings. Without it, PhotoQt cannot function!</source>
      <translation type="unfinished">You seem to be missing the SQLite driver for Qt. This is needed though for a few different things, like reading and writing the settings. Without it, PhotoQt cannot function!</translation>
    </message>
  </context>
  <context>
    <name>TabShortcuts</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/TabShortcuts.qml" line="245"/>
      <source/>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>about</name>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="52"/>
      <location filename="../qml/about/PQAbout.qml" line="165"/>
      <source>Close</source>
      <translation>Fechar</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="98"/>
      <source>Current version:</source>
      <translation>Versão atual:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="100"/>
      <source>Show configuration overview</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation type="unfinished">Show configuration overview</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="110"/>
      <source>License:</source>
      <translation>Licença:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="115"/>
      <source>Open license in browser</source>
      <translation type="unfinished">Open license in browser</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="135"/>
      <source>Open website in browser</source>
      <translation type="unfinished">Open website in browser</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="203"/>
      <source>Configuration</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation type="unfinished">Configuration</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="222"/>
      <source>Copy to clipboard</source>
      <translation type="unfinished">Copy to clipboard</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="130"/>
      <source>Website:</source>
      <translation>Site da web:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="145"/>
      <source>Contact:</source>
      <translation>Contato:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="150"/>
      <source>Send an email</source>
      <translation>Enviar um email</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAboutPopout.qml" line="34"/>
      <source>About</source>
      <extracomment>Window title</extracomment>
      <translation>Sobre</translation>
    </message>
  </context>
  <context>
    <name>advancedsort</name>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="83"/>
      <location filename="../qml/advancedsort/PQAdvancedSortPopout.qml" line="34"/>
      <source>Advanced Image Sort</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Advanced Image Sort</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="94"/>
      <source>It is possible to sort the images in the current folder by color properties. Depending on the number of images and the settings, this might take a few seconds.</source>
      <translation type="unfinished">It is possible to sort the images in the current folder by color properties. Depending on the number of images and the settings, this might take a few seconds.</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="107"/>
      <source>Sort by:</source>
      <extracomment>Used as &apos;sort by dominant/average color&apos;</extracomment>
      <translation type="unfinished">Sort by:</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="116"/>
      <source>Resolution</source>
      <extracomment>The image resolution (width/height in pixels)</extracomment>
      <translation type="unfinished">Resolution</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="118"/>
      <source>Dominant color</source>
      <extracomment>The color that is most common in the image</extracomment>
      <translation type="unfinished">Dominant color</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="120"/>
      <source>Average color</source>
      <extracomment>the average color of the image</extracomment>
      <translation type="unfinished">Average color</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="122"/>
      <source>Luminosity</source>
      <extracomment>the average color of the image</extracomment>
      <translation type="unfinished">Luminosity</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="137"/>
      <source>ascending</source>
      <extracomment>sort order, i.e., &apos;ascending order&apos;</extracomment>
      <translation type="unfinished">ascending</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="142"/>
      <source>descending</source>
      <extracomment>sort order, i.e., &apos;descending order&apos;</extracomment>
      <translation type="unfinished">descending</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="159"/>
      <source>speed vs quality:</source>
      <extracomment>Please keep short! Sorting images by color comes with a speed vs quality tradeoff.</extracomment>
      <translation type="unfinished">speed vs quality:</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="165"/>
      <source>low quality (fast)</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation type="unfinished">low quality (fast)</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="167"/>
      <source>medium quality</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation type="unfinished">medium quality</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="169"/>
      <source>high quality (slow)</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation type="unfinished">high quality (slow)</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="181"/>
      <source>There is also a quickstart shortcut that immediately starts the sorting using the latest settings.</source>
      <translation type="unfinished">There is also a quickstart shortcut that immediately starts the sorting using the latest settings.</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="201"/>
      <source>Sort images</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation type="unfinished">Sort images</translation>
    </message>
  </context>
  <context>
    <name>buttongeneric</name>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="79"/>
      <source>Ok</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Ok</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="81"/>
      <source>Cancel</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Cancelar</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="83"/>
      <source>Save</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Salvar</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="85"/>
      <source>Close</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Fechar</translation>
    </message>
  </context>
  <context>
    <name>commandlineparser</name>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="51"/>
      <source>Image Viewer</source>
      <translation>Visualizador de imagem</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="53"/>
      <source>Image file to open.</source>
      <translation>Arquivo de imagem a abrir.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="60"/>
      <source>Make PhotoQt ask for a new file.</source>
      <translation>Faça o PhotoQt perguntar por um novo arquivo.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="62"/>
      <source>Shows PhotoQt from system tray.</source>
      <translation>Mostra o PhotoQt na área de notificações.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="64"/>
      <source>Hides PhotoQt to system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Oculta PhotoQt na área de notificações.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="66"/>
      <source>Show/Hide PhotoQt.</source>
      <translation>Mostrar/ocultar PhotoQt.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="68"/>
      <source>Enable thumbnails.</source>
      <extracomment>Command line option</extracomment>
      <translation>Habilita miniaturas.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="70"/>
      <source>Disable thumbnails.</source>
      <extracomment>Command line option</extracomment>
      <translation>Desabilitar miniaturas.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="72"/>
      <source>Enable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Habilitar ícone da bandeja do sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="74"/>
      <source>Disable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Desabilitar o ícone da bandeja do sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="76"/>
      <source>Start PhotoQt hidden to the system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Iniciar PhotoQt oculto na área de notificações.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="78"/>
      <source>Open standalone PhotoQt, allows for multiple instances but without remote interaction.</source>
      <extracomment>Command line option</extracomment>
      <translation>Abrir PhotoQt sozinho, permite múltiplas instâncias, porém, sem interação remota.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="80"/>
      <source>Simulate a shortcut sequence</source>
      <extracomment>Command line option</extracomment>
      <translation>Simular a sequência de atalhos</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="82"/>
      <source>Switch on debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Ligar mensagens de depuração.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="84"/>
      <source>Switch off debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Desligar mensagens de depuração.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="86"/>
      <source>Export configuration to given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Exportar configuração to dados nome de arquivo.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="88"/>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="92"/>
      <source>filename</source>
      <extracomment>Command line option</extracomment>
      <translation>nome do arquivo</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="90"/>
      <source>Import configuration from given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Importar configuração de dado nome arquivo.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="94"/>
      <source>Check the configuration and correct any detected issues.</source>
      <extracomment>Command line option</extracomment>
      <translation type="unfinished">Check the configuration and correct any detected issues.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="96"/>
      <source>Reset default configuration.</source>
      <extracomment>Command line option</extracomment>
      <translation type="unfinished">Reset default configuration.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="98"/>
      <source>Show configuration overview.</source>
      <extracomment>Command line option</extracomment>
      <translation type="unfinished">Show configuration overview.</translation>
    </message>
  </context>
  <context>
    <name>facetagging</name>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagger.qml" line="194"/>
      <source>Who is this?</source>
      <extracomment>This question is asked in the face tagger to ask for the name of a tagged face</extracomment>
      <translation>Quem é esse?</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagger.qml" line="201"/>
      <source>Enter name</source>
      <translation>Digitar nome</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="62"/>
      <source>Click to exit face tagging mode</source>
      <translation>Clique para sair de marcação facial</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="75"/>
      <source>Click to tag faces, changes are saved automatically</source>
      <translation>Clique para marcar rostos, mudanças são salvas automaticamente</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagsUnsupported.qml" line="60"/>
      <source>File type does not support face tags.</source>
      <translation>Tipo de arquivo não suporta tags de rosto.</translation>
    </message>
  </context>
  <context>
    <name>filedialog</name>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="71"/>
      <source>Backwards</source>
      <translation>Prévio</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="94"/>
      <source>Up a level</source>
      <translation>Subir um nível</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="120"/>
      <source>Forwards</source>
      <translation>Adiante</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="187"/>
      <source>List subfolders</source>
      <translation>lista de subpastas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="265"/>
      <source>Enter fullscreen</source>
      <translation>Entrar em tela-cheia</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="266"/>
      <source>Exit fullscreen</source>
      <translation>Sair da tela-cheia</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="295"/>
      <source>Close</source>
      <translation>Fechar</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQDevices.qml" line="109"/>
      <source>Storage devices</source>
      <extracomment>This is the category title of storage devices to open (like USB keys) in the element for opening files</extracomment>
      <translation>Dispositivos de armazenamento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQDevices.qml" line="141"/>
      <source>Detected storage devices on your system</source>
      <translation>Dispositivos de armazenamento detectados no seu sistema</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="118"/>
      <source>no supported files/folders found</source>
      <translation>Nenhum arquivos/pastas suportados foram encontrados</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="232"/>
      <source>Click and drag to favorites</source>
      <translation>Clique e arraste para os favoritos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="359"/>
      <source># images</source>
      <translation># imagens</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="360"/>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="379"/>
      <source>Date:</source>
      <translation>Data:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="361"/>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="380"/>
      <source>Time:</source>
      <translation>Tempo:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="377"/>
      <source>File size:</source>
      <translation>Tamanho do arquivo:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="378"/>
      <source>File type:</source>
      <translation>Tipo do arquivo:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="462"/>
      <source>%1 image</source>
      <translation>%1 Imagem</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="464"/>
      <source>%1 images</source>
      <translation>%1 Imagens</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="50"/>
      <source>no folder added to favorites yet</source>
      <extracomment>&apos;favorites&apos; here refers to the list of favorite folders a user can set in the file dialog</extracomment>
      <translation>nenhuma pasta adicionada aos favoritos ainda</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="156"/>
      <source>Favorites</source>
      <extracomment>This is the category title of user-set folders (or favorites) in the file dialog</extracomment>
      <translation>Favoritos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="172"/>
      <source>Your favorites</source>
      <translation>Seus Favoritos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="222"/>
      <source>Show entry</source>
      <translation>Mostrar entrada</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="222"/>
      <source>Hide entry</source>
      <translation>Ocultar entrada</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="223"/>
      <source>Remove entry</source>
      <translation>Remover entrada</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="224"/>
      <source>Hide hidden entries</source>
      <translation>Escoder entradas ocultas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="224"/>
      <source>Show hidden entries</source>
      <translation>Mostrar entradas ocultas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="43"/>
      <source>Load this folder</source>
      <translation>Carregar esta pasta</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="43"/>
      <source>Load this file</source>
      <translation>Carregar este arquivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="56"/>
      <source>Add to Favorites</source>
      <translation>Adicionar aos favoritos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="78"/>
      <source>Show tooltip with image details</source>
      <translation type="unfinished">Show tooltip with image details</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="88"/>
      <source>Visible</source>
      <extracomment>This is a context menu entry, referring to whether the large preview image is VISIBLE</extracomment>
      <translation type="unfinished">Visible</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="96"/>
      <source>Higher resolution</source>
      <extracomment>This is a context menu entry, referring to whether a preview image with a HIGHER RESOLUTION should be loaded</extracomment>
      <translation type="unfinished">Higher resolution</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="104"/>
      <source>Blurred image</source>
      <extracomment>This is a context menu entry, selecting it will BLUR the preview IMAGE</extracomment>
      <translation type="unfinished">Blurred image</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="112"/>
      <source>Muted colors</source>
      <extracomment>This is a context menu entry, selecting it will make the COLORS of the preview image MUTED</extracomment>
      <translation type="unfinished">Muted colors</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="120"/>
      <source>Full colors</source>
      <extracomment>This is a context menu entry, selecting it will make the COLORS of the preview image in FULL (i.e., without blur/opacity)</extracomment>
      <translation type="unfinished">Full colors</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="64"/>
      <source>Show hidden files</source>
      <translation>Mostrar arquivos ocultos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="71"/>
      <source>Show thumbnails</source>
      <translation>Mostrar miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQStandard.qml" line="103"/>
      <source>Standard</source>
      <extracomment>This is the category title of user-set folders (or favorites) in the file dialog</extracomment>
      <translation>Padrão</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQStandard.qml" line="119"/>
      <source>Some standard locations</source>
      <translation>Alguns locais padrão</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="48"/>
      <source>Zoom:</source>
      <translation>Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="57"/>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="72"/>
      <source>Adjust font size of files and folders</source>
      <translation>Ajustar tamanho da fonte de arquivos e pastas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="73"/>
      <source>Zoom factor:</source>
      <translation>Nível de Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="88"/>
      <source>Sort by:</source>
      <translation>Ordenar por:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="90"/>
      <source>Name</source>
      <translation>Nome</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="91"/>
      <source>Natural Name</source>
      <translation>Nome natural</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="92"/>
      <source>Time modified</source>
      <translation>Tempo modificado</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="93"/>
      <source>File size</source>
      <translation>Tamanho do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="94"/>
      <source>File type</source>
      <translation>Tipo de arquivo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="95"/>
      <source>reverse order</source>
      <translation>Ordem reversa</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="102"/>
      <source>Choose by what to sort the files</source>
      <translation>Escolher por qual ordenar os arquivos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="138"/>
      <source>All supported images</source>
      <translation>Todas as imagens suportadas</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="143"/>
      <source>Video files</source>
      <translation>Arquivos de vídeo</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="144"/>
      <source>All files</source>
      <translation>Todos os arquivos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="154"/>
      <source>Choose which selection of files to show</source>
      <translation>Escolher qual seleção de arquivos mostrar</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="174"/>
      <source>Remember loaded folder between sessions.</source>
      <translation>Lembrar pasta carregada entre sessões.</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="196"/>
      <source>Switch between list and icon view</source>
      <translation>Alternar entre visão de lista e ícone</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="116"/>
      <source>Hide standard locations</source>
      <translation>Esconder locais padrão</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="116"/>
      <source>Show standard locations</source>
      <translation>Mostrar locais padrão</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="117"/>
      <source>Hide favorite locations</source>
      <translation>Esconder locais favoritos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="117"/>
      <source>Show favorite locations</source>
      <translation>Mostrar locais favoritos</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="118"/>
      <source>Hide storage devices</source>
      <translation>Ocultar dispositivos de armazenamento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="118"/>
      <source>Show storage devices</source>
      <translation>Mostrar dispositivos de armazenamento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialogPopout.qml" line="34"/>
      <source>File dialog</source>
      <extracomment>Window title</extracomment>
      <translation>Arquivo de diálogo</translation>
    </message>
  </context>
  <context>
    <name>filemanagement</name>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="83"/>
      <location filename="../qml/filemanagement/PQDeletePopout.qml" line="34"/>
      <source>Delete file?</source>
      <extracomment>Window title</extracomment>
      <translation>Deletar arquivo?</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="101"/>
      <source>An error occured, file could not be deleted!</source>
      <translation>Um erro ocorreu, arquivo não pode ser deletado!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="121"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="177"/>
      <source>Move to trash</source>
      <translation>Mover para a lixeira</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="137"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="180"/>
      <source>Delete permanently</source>
      <translation>Deletar permanentemente</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="83"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="132"/>
      <location filename="../qml/filemanagement/PQRenamePopout.qml" line="34"/>
      <source>Rename file</source>
      <extracomment>Window title</extracomment>
      <translation>Renomear arquivo</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="101"/>
      <source>An error occured, file could not be renamed!</source>
      <translation>Um erro ocorreu, arquivo não pode ser renomeado!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="112"/>
      <source>Enter new filename</source>
      <translation>Digitar novo nome de arquivo</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="82"/>
      <location filename="../qml/filemanagement/PQSaveAsPopout.qml" line="34"/>
      <source>Save file as</source>
      <extracomment>This is a title, similar to all the &apos;save as&apos; options in many programs.
----------
Window title</extracomment>
      <translation>Salvar arquivo como</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="105"/>
      <source>An error occured, file could not be saved!</source>
      <translation>An erro ocorreu, arquivo não pode ser salvo!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="116"/>
      <source>Operation cancelled</source>
      <extracomment>&apos;Operation&apos; here is the operation of saving an image in a new format</extracomment>
      <translation>Operação cancelada</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="124"/>
      <source>Filter formats</source>
      <extracomment>This is a short hint informing the user that here they can &apos;filter all the possible file formats&apos;</extracomment>
      <translation>Filtrar formatos</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="223"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="230"/>
      <source>New filename</source>
      <translation>Novo nome de arquivo</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="251"/>
      <source>Choose location and save file</source>
      <translation>Escolher local e salvar o arquivo</translation>
    </message>
  </context>
  <context>
    <name>filter</name>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="90"/>
      <source>Filter images in current directory</source>
      <translation>Filtrar imagens no diretório atual</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="102"/>
      <source>To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</source>
      <translation type="unfinished">To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="120"/>
      <source>File name/extension:</source>
      <translation type="unfinished">File name/extension:</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="132"/>
      <source>Enter terms</source>
      <translation type="unfinished">Enter terms</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="161"/>
      <location filename="../qml/filter/PQFilter.qml" line="213"/>
      <source>greater than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution GREATER THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size GREATER THAN 123 KB/MB&apos;</extracomment>
      <translation type="unfinished">greater than</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="163"/>
      <location filename="../qml/filter/PQFilter.qml" line="215"/>
      <source>less than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution LESS THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size LESS THAN 123 KB/MB&apos;</extracomment>
      <translation type="unfinished">less than</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="200"/>
      <source>File size</source>
      <translation type="unfinished">File size</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="260"/>
      <source>Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</source>
      <translation type="unfinished">Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="283"/>
      <location filename="../qml/filter/PQFilterPopout.qml" line="34"/>
      <source>Filter</source>
      <extracomment>Written on a clickable button - please keep short
----------
Window title</extracomment>
      <translation>Filtrar</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="305"/>
      <source>Remove filter</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Remover filtro</translation>
    </message>
  </context>
  <context>
    <name>histogram</name>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="143"/>
      <location filename="../qml/histogram/PQHistogramPopout.qml" line="34"/>
      <source>Histogram</source>
      <extracomment>Window title</extracomment>
      <translation>Histograma</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="145"/>
      <source>Loading...</source>
      <extracomment>As in: Loading the histogram for the current image</extracomment>
      <translation>Carregando...</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="175"/>
      <source>Click-and-drag to move.</source>
      <extracomment>Used for the histogram. The version refers to the type of histogram that is available (colored and greyscale)</extracomment>
      <translation>Clicar e arrastar para mover.</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="175"/>
      <source>Right click to switch version.</source>
      <translation>Clique com o botão direito para alternar a versão.</translation>
    </message>
  </context>
  <context>
    <name>imageprovider</name>
    <message>
      <location filename="../cplusplus/imageprovider/imageproviderfull.cpp" line="65"/>
      <location filename="../cplusplus/imageprovider/imageproviderthumb.cpp" line="147"/>
      <source>File failed to load, it does not exist!</source>
      <translation>Falha ao carregar o arquivo, ele não existe!</translation>
    </message>
  </context>
  <context>
    <name>imgur</name>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="86"/>
      <location filename="../qml/imgur/PQImgurPopout.qml" line="34"/>
      <source>Upload to imgur.com</source>
      <extracomment>Window title</extracomment>
      <translation>Enviar para imgur.com</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="97"/>
      <source>anonymous</source>
      <extracomment>Used as in: Upload image as anonymous user</extracomment>
      <translation>Anônimo</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="129"/>
      <source>Obtaining image url...</source>
      <translation>Obtendo url da imagem...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="140"/>
      <source>This seems to take a long time...</source>
      <translation>Isto parece levar um tempão...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="141"/>
      <source>There might be a problem with your internet connection or the imgur.com servers.</source>
      <translation>Pode haver um problema com sua conexão de internet ou com os servidores de imgur.com.</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="152"/>
      <source>An Error occurred while uploading image!</source>
      <translation>Um erro ocorreu no envio da imagem!</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="153"/>
      <source>Error code:</source>
      <translation>código de erro:</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="164"/>
      <source>You do not seem to be connected to the internet...</source>
      <translation>Você não parece estar conectado a internet...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="165"/>
      <source>Unable to upload!</source>
      <translation>Incapaz de enviar!</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="187"/>
      <source>Access Image</source>
      <translation>Acessar imagem</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="200"/>
      <location filename="../qml/imgur/PQImgur.qml" line="232"/>
      <source>Click to open in browser</source>
      <translation>Clique para abrir no navegador</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="207"/>
      <location filename="../qml/imgur/PQImgur.qml" line="239"/>
      <source>Copy to clipboard</source>
      <translation>Copiar para a Área de transferência</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="219"/>
      <source>Delete Image</source>
      <translation>Deletar imagem</translation>
    </message>
  </context>
  <context>
    <name>keymouse</name>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="30"/>
      <source>Alt</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Alt</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="32"/>
      <source>Ctrl</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Ctrl</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="34"/>
      <source>Shift</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Shift</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="36"/>
      <source>Page Up</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Page Up</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="38"/>
      <source>Page Down</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Page Down</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="40"/>
      <source>Meta</source>
      <extracomment>Refers to the key that usually has the Windows symbol on it</extracomment>
      <translation>Meta</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="42"/>
      <source>Keypad</source>
      <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
      <translation>Teclado numérico</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="44"/>
      <source>Escape</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Escape</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="46"/>
      <source>Right</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Direita</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="48"/>
      <source>Left</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Esquerda</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="50"/>
      <source>Up</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Cima</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="52"/>
      <source>Down</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Baixo</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="54"/>
      <source>Space</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Espaço</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="56"/>
      <source>Delete</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Deletar</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="58"/>
      <source>Backspace</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Barra de espaço</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="60"/>
      <source>Home</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Home</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="62"/>
      <source>End</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>End</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="64"/>
      <source>Insert</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Inserir</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="66"/>
      <source>Tab</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Tab</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="69"/>
      <source>Return</source>
      <extracomment>Return refers to the enter key of the number block - please try to make the translations of Return and Enter (the main button) different if possible!</extracomment>
      <translation>Retornar</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="72"/>
      <source>Enter</source>
      <extracomment>Enter refers to the main enter key - please try to make the translations of Return (in the number block) and Enter different if possible!</extracomment>
      <translation>Enter</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="78"/>
      <source>Left Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botão esquerdo</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="80"/>
      <source>Right Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botão direito</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="82"/>
      <source>Middle Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Botão do meio</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="84"/>
      <source>Double Click</source>
      <extracomment>Refers to a mouse event</extracomment>
      <translation type="unfinished">Double Click</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="86"/>
      <source>Wheel Up</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Rolar para cima</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="88"/>
      <source>Wheel Down</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Rolar para baixo</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="90"/>
      <source>East</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Leste</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="92"/>
      <source>South</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Sul</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="94"/>
      <source>West</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Oeste</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="96"/>
      <source>North</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Norte</translation>
    </message>
  </context>
  <context>
    <name>logging</name>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="33"/>
      <source>Logging</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Logging</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="109"/>
      <source>enable debug messages</source>
      <translation type="unfinished">enable debug messages</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="134"/>
      <source>copy to clipboard</source>
      <translation type="unfinished">copy to clipboard</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="135"/>
      <source>save to file</source>
      <translation type="unfinished">save to file</translation>
    </message>
  </context>
  <context>
    <name>metadata</name>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="90"/>
      <source>File name</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Nome do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="92"/>
      <source>Dimensions</source>
      <extracomment>The dimensions of the loaded image. Please keep string short!</extracomment>
      <translation>Dimensões</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="96"/>
      <source>File size</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Tamanho do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="98"/>
      <source>File type</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Tipo do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="101"/>
      <source>Make</source>
      <extracomment>Exif image metadata: the make of the camera used to take the photo. Please keep string short!</extracomment>
      <translation>Criar</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="103"/>
      <source>Model</source>
      <extracomment>Exif image metadata: the model of the camera used to take the photo. Please keep string short!</extracomment>
      <translation>Model</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="105"/>
      <source>Software</source>
      <extracomment>Exif image metadata: the software used to create the photo. Please keep string short!</extracomment>
      <translation>Sofware</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="108"/>
      <source>Time Photo was Taken</source>
      <extracomment>Exif image metadata: when the photo was taken. Please keep string short!</extracomment>
      <translation>O tempo que a foto foi tirada</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="110"/>
      <source>Exposure Time</source>
      <extracomment>Exif image metadata: how long the sensor was exposed to the light. Please keep string short!</extracomment>
      <translation>Tempo de exposição</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="112"/>
      <source>Flash</source>
      <extracomment>Exif image metadata: the flash setting when the photo was taken. Please keep string short!</extracomment>
      <translation>Flash</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="115"/>
      <source>Scene Type</source>
      <extracomment>Exif image metadata: the specific scene type the camera used for the photo. Please keep string short!</extracomment>
      <translation>Tipo de cena</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="117"/>
      <source>Focal Length</source>
      <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/Focal_length . Please keep string short!</extracomment>
      <translation>Distância focal</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="160"/>
      <location filename="../qml/menumeta/PQMetaDataPopout.qml" line="34"/>
      <source>Metadata</source>
      <extracomment>This is the heading of the metadata element
----------
Window title</extracomment>
      <translation>Metadados</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="199"/>
      <source>No File Loaded</source>
      <translation>Nenhum arquivo carregado</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="252"/>
      <source>Click to open GPS position with online map</source>
      <translation>Clique para abrir a posição da GPS com mapa online</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="353"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="388"/>
      <source>Click and drag to resize element</source>
      <translation type="unfinished">Click and drag to resize element</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="121"/>
      <source>Light Source</source>
      <extracomment>Exif image metadata: What type of light the camera detected. Please keep string short!</extracomment>
      <translation>Fonte de luz</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="94"/>
      <source>Image</source>
      <extracomment>Used as in &quot;Image 3/16&quot;. The numbers (position of image in folder) are added on automatically. Please keep string short!</extracomment>
      <translation type="unfinished">Image</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="124"/>
      <source>Keywords</source>
      <extracomment>IPTC image metadata: A description of the image by the user/software. Please keep string short!</extracomment>
      <translation>Palavras-chave</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="126"/>
      <source>Location</source>
      <extracomment>IPTC image metadata: The CITY and COUNTRY the imge was taken in. Please keep string short!</extracomment>
      <translation>Localização</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="128"/>
      <source>Copyright</source>
      <extracomment>IPTC image metadata. Please keep string short!</extracomment>
      <translation>Direitos atuais</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="131"/>
      <source>GPS Position</source>
      <extracomment>Exif image metadata. Please keep string short!</extracomment>
      <translation>Posição de GPS</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="119"/>
      <source>F Number</source>
      <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/F-number . Please keep string short!</extracomment>
      <translation>Número F</translation>
    </message>
  </context>
  <context>
    <name>navigate</name>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="66"/>
      <source>Click and drag to move</source>
      <translation>Clicar e arrastar para mover</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="99"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="71"/>
      <source>Navigate to previous image in folder</source>
      <translation>Navegar para a imagem prévia na pasta</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="128"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="93"/>
      <source>Navigate to next image in folder</source>
      <translation>Navegar para a seguinte imagem na pasta</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="154"/>
      <source>Show main menu</source>
      <translation>Mostrar menu principal</translation>
    </message>
  </context>
  <context>
    <name>other</name>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="76"/>
      <source>Open a file to start</source>
      <translation>Abrir um arquivo para iniciar</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="114"/>
      <source>Click anywhere to open a file</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation type="unfinished">Click anywhere to open a file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="124"/>
      <source>Move your cursor to:</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation type="unfinished">Move your cursor to:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="134"/>
      <source>RIGHT EDGE for the main menu</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, first option for where to move cursor to</extracomment>
      <translation type="unfinished">RIGHT EDGE for the main menu</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="145"/>
      <source>LEFT EDGE for the metadata</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, second option for where to move cursor to</extracomment>
      <translation type="unfinished">LEFT EDGE for the metadata</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="156"/>
      <source>BOTTOM EDGE to show the thumbnails</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, third option for where to move cursor to</extracomment>
      <translation type="unfinished">BOTTOM EDGE to show the thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="166"/>
      <source>(once an image/folder is loaded)</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation type="unfinished">(once an image/folder is loaded)</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="221"/>
      <source>No matches found</source>
      <extracomment>Used as in: No matches found for the currently set filter</extracomment>
      <translation>Nenhum resultado encotrado</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="233"/>
      <source>PhotoQt Image Viewer</source>
      <extracomment>The window title of PhotoQt</extracomment>
      <translation>Visualizador de imagem PhotoQt</translation>
    </message>
  </context>
  <context>
    <name>popinpopout</name>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="270"/>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="295"/>
      <location filename="../qml/chromecast/PQChromecast.qml" line="289"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="231"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="230"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="213"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="345"/>
      <location filename="../qml/filter/PQFilter.qml" line="361"/>
      <location filename="../qml/histogram/PQHistogram.qml" line="298"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="426"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="304"/>
      <location filename="../qml/scale/PQScale.qml" line="329"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="367"/>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="532"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="403"/>
      <source>Merge into main interface</source>
      <extracomment>Tooltip of small button to merge a popped out element (i.e., one in its own window) into the main interface</extracomment>
      <translation>Mesclar na interface principal</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="272"/>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="297"/>
      <location filename="../qml/chromecast/PQChromecast.qml" line="291"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="233"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="232"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="215"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="347"/>
      <location filename="../qml/filter/PQFilter.qml" line="363"/>
      <location filename="../qml/histogram/PQHistogram.qml" line="300"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="428"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="306"/>
      <location filename="../qml/scale/PQScale.qml" line="331"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="369"/>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="534"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="405"/>
      <source>Move to its own window</source>
      <extracomment>Tooltip of small button to show an element in its own window (i.e., not merged into main interface)</extracomment>
      <translation>Mover para sua própria janela</translation>
    </message>
  </context>
  <context>
    <name>quickinfo</name>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="122"/>
      <source>Click here to show main menu</source>
      <translation type="unfinished">Click here to show main menu</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="151"/>
      <source>Click here to enter fullscreen mode</source>
      <translation>Clique aqui para entrar no modo tela cheia</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="152"/>
      <source>Click here to exit fullscreen mode</source>
      <translation>Clique aqui para sair do modo tela cheia</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="190"/>
      <source>Click here to close PhotoQt</source>
      <translation>Clique aqui para fechar o PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="213"/>
      <source>Show counter</source>
      <translation>Mostrar contador</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="214"/>
      <source>Hide counter</source>
      <translation>Esconder contador</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="216"/>
      <source>Show file path</source>
      <translation>Mostrar caminho de arquivo</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="217"/>
      <source>Hide file path</source>
      <translation>Esconder caminho do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="219"/>
      <source>Show file name</source>
      <translation>Mostrar caminho do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="220"/>
      <source>Hide file name</source>
      <translation>Esconder nome do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="222"/>
      <source>Show zoom level</source>
      <translation>Mostrar nível de zoom</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="223"/>
      <source>Hide zoom level</source>
      <translation>Esconder nível de zoom</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQImage.qml" line="712"/>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="388"/>
      <source>Click here to enter viewer mode</source>
      <translation>Clique aqui para entrar no modo visualizador</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQImage.qml" line="731"/>
      <source>Hide central &apos;viewer mode&apos; button</source>
      <translation type="unfinished">Hide central &apos;viewer mode&apos; button</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="238"/>
      <source>Some information about the current image and directory</source>
      <translation type="unfinished">Some information about the current image and directory</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="289"/>
      <source>Connected to:</source>
      <extracomment>This is followed by the name of the Chromecast streaming device currently connected to</extracomment>
      <translation type="unfinished">Connected to:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="387"/>
      <source>Click here to exit viewer mode</source>
      <translation>Clique aqui para sair do modo visualizador</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="342"/>
      <source>Filter:</source>
      <translation>Filtro:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="225"/>
      <source>Show window buttons</source>
      <translation>Mostrar botẽs da janela</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="226"/>
      <source>Hide window buttons</source>
      <translation>Esconder botões da janela</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="329"/>
      <source>Click to remove filter</source>
      <translation>Clique para remover filtro</translation>
    </message>
  </context>
  <context>
    <name>scale</name>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="84"/>
      <location filename="../qml/scale/PQScalePopout.qml" line="34"/>
      <source>Scale file</source>
      <extracomment>Window title</extracomment>
      <translation>Arquivo de escala</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="94"/>
      <source>An error occured, file could not be scaled!</source>
      <translation>Um erro ocorreu, arquivo não poder ser escalado!</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="104"/>
      <source>This file format cannot (yet) be scaled with PhotoQt!</source>
      <translation>Este formato de arquivo não pode (ainda) ser escalado com PhotoQt!</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="115"/>
      <source>New width x height:</source>
      <translation>Nova largura x altura:</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="188"/>
      <source>Preserve aspect ratio</source>
      <extracomment>The aspect ratio refers to the ratio of the width to the height of the image, e.g., 16:9 for most movies</extracomment>
      <translation>Preservar raio de aspecto</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="199"/>
      <source>Quality:</source>
      <extracomment>This refers to the quality to be used to scale the image</extracomment>
      <translation>Qualidade:</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="232"/>
      <location filename="../qml/scale/PQScale.qml" line="302"/>
      <source>Scale (create new file)</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Escalar (criar novo arquivo)</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="260"/>
      <location filename="../qml/scale/PQScale.qml" line="303"/>
      <source>Scale (change file in place)</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Escalar (alterar arquivo no local)</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="304"/>
      <source>De-/Increase width and height by 10%</source>
      <translation>Diminuir/Aumentar largura e altura por 10%</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="305"/>
      <source>In-/Decrease quality by 5%</source>
      <translation>Aumentar/Diminuir qualidade por 5%</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/handlingmanipulation.cpp" line="233"/>
      <source>Select new file</source>
      <translation>Selecionar novo arquivo</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager</name>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="100"/>
      <source>interface</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Interface</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="101"/>
      <source>Tab to control interface settings</source>
      <translation>Aba para controlar configurações de interface</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="103"/>
      <source>image view</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Visualização de imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="104"/>
      <source>Tab to control how images are viewed</source>
      <translation>Aba para controlar como imagens são visualizadas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="106"/>
      <source>thumbnails</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="107"/>
      <source>Tab to control the look and behaviour of thumbnails</source>
      <translation>Aba para controlar o visual e comportamento das miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="109"/>
      <source>metadata</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Metadados</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="110"/>
      <source>Tab to control metadata settings</source>
      <translation>Aba para controlar configurações de metadados</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="112"/>
      <source>file types</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Tipos de arquivos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="113"/>
      <source>Tab to control which file types PhotoQt should recognize</source>
      <translation>Aba para controlar quais tipos de arquivos o PhotoQt deveria reconhecer</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="115"/>
      <source>shortcuts</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Atalhos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="116"/>
      <source>Tab to control which shortcuts are set</source>
      <translation>Aba para controlar quais atalhos estão definidos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="224"/>
      <source>advanced</source>
      <extracomment>Written on button in setting manager. A click on this button opens a menu with some advanced actions.</extracomment>
      <translation>Avançado</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="228"/>
      <source>restore defaults</source>
      <translation>Restaurar padrões</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="229"/>
      <source>import settings</source>
      <translation>Importar configurações</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="230"/>
      <source>export settings</source>
      <translation>Exportar configurações</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>disable expert mode</source>
      <translation>Desabilitar modo avançado</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>enable expert mode</source>
      <translation>Habilitar modo avançado</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="272"/>
      <source>Import of %1. This will replace your current settings with the ones stored in the backup.</source>
      <translation>Importar de %1. Isso substituirá suas configurações atuais com aquelas armazenadas no backup.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="273"/>
      <source>Do you want to continue?</source>
      <translation>Você quer continuar?</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="325"/>
      <source>Save changes and exit</source>
      <translation>Salvar mudanças e sair</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="336"/>
      <source>Exit and discard changes</source>
      <translation>Sair e descartar mudanças</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="177"/>
      <source>Rename File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Renomear arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="179"/>
      <source>Delete File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Deletar arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="76"/>
      <source>Filetype settings</source>
      <translation>Configurações de tipo de arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="90"/>
      <source>These settings govern which file types PhotoQt should recognize and open.</source>
      <translation>Essas configurações governam quais tipos de arquivos PhotoQt deveria reconhecer e abrir.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="90"/>
      <source>Not all file types might be available, depending on your setup and what library support was enabled at compile time</source>
      <translation>Nem todos os tipos de arquivos estão disponíveis, dependendo da sua configuração e qual suporte de biblioteca estava habilitado na hora de compilação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="84"/>
      <source>Image view settings</source>
      <translation>Configurações de visualização de imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="98"/>
      <source>These settings affect the viewing of images, how they are shown, in what order, how large a cache to use, etc.</source>
      <translation>Essas configurações afetam a visualização das imagens, como elas são mostradas, em qual ordem, quão grande o cache para usar, etc.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="98"/>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="100"/>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="99"/>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="99"/>
      <source>Some settings are only shown in expert mode.</source>
      <translation>Algumas configurações são mostradas somente em modo experiente.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="86"/>
      <source>Interface settings</source>
      <translation>Configurações de interface</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="100"/>
      <source>These settings affect the interface in general, how the application looks like and behaves. This includes the background, some of the labels in the main view, which elements are to be shown in their own window, and others.</source>
      <translation>Essas configurações afetam a interface em geral, como a aplicação aparenta e se comporta. Isso inclui o background, alguns dos rótulos na visão principal, quais elementos estão para ser mostrados in sua própria janela e outras.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="85"/>
      <source>Metadata settings</source>
      <translation>Configurações de metadados</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="99"/>
      <source>These settings affect the metadata element, what information it should show and some of its behavior.</source>
      <translation>Essas configurações afetam o elemento de metadados, quais informações ele deverá mostrar e algum dos seus comportamentos.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="77"/>
      <source>Shortcuts</source>
      <translation>Atalhos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="101"/>
      <source>Navigation</source>
      <extracomment>A shortcuts category: navigation</extracomment>
      <translation>Navegação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="107"/>
      <source>Filter images in folder</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Filtrar imagens na pasta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="109"/>
      <source>Next image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Imagem seguinte</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="111"/>
      <source>Previous image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Imagem prévia</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="115"/>
      <source>Go to first image</source>
      <extracomment>Name of shortcut action Name of shortcut action</extracomment>
      <translation>Ir para primeira imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="117"/>
      <source>Go to last image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ir para última imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="119"/>
      <source>Enter viewer mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Entrar no modo visualizador</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="123"/>
      <source>Close window (hides to system tray if enabled)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Fechar janela (ocultar para a bandeja do sistema se habilitado)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="125"/>
      <source>Quit PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Sair PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="133"/>
      <source>Image</source>
      <extracomment>A shortcuts category: image manipulation</extracomment>
      <translation>Imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="137"/>
      <source>Zoom In</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Aproximar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="139"/>
      <source>Zoom Out</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Afastar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="141"/>
      <source>Zoom to Actual Size</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Dimensionar para o tamanho real</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="143"/>
      <source>Reset Zoom</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Redefinir Zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="145"/>
      <source>Toggle &apos;Fit in window&apos;</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Toggle &apos;Fit in window&apos;</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="147"/>
      <source>Rotate Right</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Girar à Direita</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="149"/>
      <source>Rotate Left</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Girar à Esquerda</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="151"/>
      <source>Reset Rotation</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Redefinir Rotação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="153"/>
      <source>Flip Horizontally</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Virar Horizontalmente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="155"/>
      <source>Flip Vertically</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Virar Verticalmente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="157"/>
      <source>Scale Image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Dimensionar imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="159"/>
      <source>Play/Pause animation/video</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Reproduzir/Pausar animação/Vídeo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="161"/>
      <source>Hide/Show face tags (stored in metadata)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Esconder/Mostrar etiquetas faciais (armazenadas nos metatados)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="165"/>
      <source>Advanced image sort (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Advanced image sort (Quickstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="213"/>
      <source>Start Slideshow (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Start Slideshow (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="243"/>
      <source>filename including path</source>
      <translation type="unfinished">filename including path</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="244"/>
      <source>filename without path</source>
      <translation type="unfinished">filename without path</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="245"/>
      <source>directory containing file</source>
      <translation type="unfinished">directory containing file</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="173"/>
      <source>File</source>
      <extracomment>A shortcuts category: file management</extracomment>
      <translation>Arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="121"/>
      <source>Show floating navigation buttons</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show floating navigation buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="181"/>
      <source>Delete File (without confirmation)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Deletar Arquivo (Sem confirmação)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="183"/>
      <source>Copy File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Copiar Arquivo para um local novo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="185"/>
      <source>Move File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mover arquivo para um local novo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="187"/>
      <source>Copy Image to Clipboard</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Copiar imagem para área de transferência</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="191"/>
      <source>Print current photo</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Print current photo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="199"/>
      <source>Other</source>
      <extracomment>A shortcuts category: other functions</extracomment>
      <translation>Outro</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="203"/>
      <source>Hide/Show main menu</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Esconder/Mostrar menu principal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="205"/>
      <source>Hide/Show metadata</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Esconder/Mostrar metadado</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="207"/>
      <source>Keep metadata opened</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Manter metadado aberto</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="209"/>
      <source>Hide/Show thumbnails</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Esconder/Mostrar miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="211"/>
      <source>Show Settings</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mostrar configurações</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="215"/>
      <source>Start Slideshow (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Iniciar apresentação de slides (Início rápido)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="217"/>
      <source>About PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Sobre PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="219"/>
      <source>Set as Wallpaper</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Definir como papel de parede</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="221"/>
      <source>Show Histogram</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Mostrar Histograma</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="223"/>
      <source>Upload to imgur.com (anonymously)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Enviar imagem para imgur.com (anonimamente)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="225"/>
      <source>Upload to imgur.com user account</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Enviar imagem para conta de usuário imgur.com</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="227"/>
      <source>Stream content to Chromecast device</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Stream content to Chromecast device</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="229"/>
      <source>Show log/debug messages</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show log/debug messages</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="231"/>
      <source>Toggle fullscreen mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Toggle fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="241"/>
      <source>External</source>
      <extracomment>A shortcuts category: external shortcuts</extracomment>
      <translation>Externo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="85"/>
      <source>Thumbnails settings</source>
      <translation>Configurações de miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="99"/>
      <source>These settings affect the thumbnails shown, by default, along the bottom edge of the screen. This includes their look, behavior, and the user&apos;s interaction with them.</source>
      <translation>Essas configurações afetam miniaturas mostrada, por padrão, ao longo da borda inferior da tela. Isso inclui sua aparência, comportamento, e o usuário%apos;s interação com elas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManagerPopout.qml" line="34"/>
      <source>Settings Manager</source>
      <extracomment>Window title</extracomment>
      <translation>Gerente de configurações</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="91"/>
      <source>Here the shortcuts can be managed. Below you can add a new shortcut for any one of the available actions, both key combinations and mouse gestures are supported.</source>
      <translation>Aqui os atalhos podem ser geridos. Abaixo, você pode adicionar um atalho novo para qualquer uma das ações disponível, ambas combinações de teclas e gestos do mouse são suportados.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="91"/>
      <source>You can also set the same shortcut for multiple actions or multiple times for the same action. All actions for a shortcut will be executed sequentially, allowing a lot more flexibility in using PhotoQt.</source>
      <translation>Você também pode definir o atalho para múltiplas ações ou múltiplas vezes para a mesma ação. Todas as ações para um atalho serão executadas sequencialmente, permitindo muito mais flexibilidade no uso do PhotoQt.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="105"/>
      <source>Open file (browse images)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Open file (browse images)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="163"/>
      <source>Advanced image sort (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Advanced image sort (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="189"/>
      <source>Save image in another format</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Salvar imagem em outro formato</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="72"/>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="128"/>
      <source>Restore defaults</source>
      <extracomment>As in &apos;restore the default settings and/or file formats and/or shortcuts&apos;. Please keep short!</extracomment>
      <translation>Restaurar padrões</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="83"/>
      <source>Here you can restore the default configuration of PhotoQt. You can choose to restore any combination of the following three categories.</source>
      <translation>Aqui você pode restaurar a configuração padrão do PhotoQt. Você pode escolher restaurar qualquer combinação das três categorias seguintes.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="96"/>
      <source>Restore default settings</source>
      <translation>Restaurar configurações padrão</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="103"/>
      <source>Restore default file formats</source>
      <translation>Restaurar formatos de arquivos padrão</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="109"/>
      <source>Restore default shortcuts</source>
      <translation>Restaurar atalhos Padrão</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_filetypes</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQLibArchive.qml" line="32"/>
      <source>These are some additional settings for opening archives.</source>
      <translation>Estas são algumas configurações adicionais para abertura de arquivos.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQLibArchive.qml" line="45"/>
      <source>use external tool: unrar</source>
      <extracomment>used as label for checkbox</extracomment>
      <translation>Usar ferramenta externa: unrar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="32"/>
      <source>These are some additional settings for showing PDFs.</source>
      <translation>Essas são algumas configurações adicionais para mostrar PDFs.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="44"/>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="55"/>
      <source>Quality:</source>
      <extracomment>the quality setting to be used when loading PDFs</extracomment>
      <translation>Qualidade:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="32"/>
      <source>These are some additional settings for playing videos.</source>
      <translation>Essas são algumas configurações adicionais para reproduzir vídeos.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="45"/>
      <source>Autoplay</source>
      <extracomment>Used as setting for video files (i.e., autoplay videos)</extracomment>
      <translation>Reprodução automática</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="51"/>
      <source>Loop</source>
      <extracomment>Used as setting for video files (i.e., loop videos)</extracomment>
      <translation>Repetir</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="58"/>
      <source>Prefer libmpv</source>
      <extracomment>Used as setting for video files</extracomment>
      <translation type="unfinished">Prefer libmpv</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="59"/>
      <source>libmpv can offer a more comprehensive video support but tends to be slightly slower to load.</source>
      <translation type="unfinished">libmpv can offer a more comprehensive video support but tends to be slightly slower to load.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="64"/>
      <source>Select tool for creating video thumbnails</source>
      <extracomment>Tooltip shown for combobox for selectiong video thumbnailer</extracomment>
      <translation>Selecionar ferramentar para criar miniaturas de vídeo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="55"/>
      <source>images</source>
      <extracomment>This is a category of files PhotoQt can recognize: any image format</extracomment>
      <translation>Imagens</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="57"/>
      <source>compressed files</source>
      <extracomment>This is a category of files PhotoQt can recognize: compressed files like zip, tar, cbr, 7z, etc.</extracomment>
      <translation>Arquivos comprimidos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="59"/>
      <source>documents</source>
      <extracomment>This is a category of files PhotoQt can recognize: documents like pdf, txt, etc.</extracomment>
      <translation>Documentos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="61"/>
      <source>videos</source>
      <extracomment>This is a type of category of files PhotoQt can recognize: videos like mp4, avi, etc.</extracomment>
      <translation>Vídeos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="67"/>
      <source>Enable</source>
      <extracomment>As in: &quot;Enable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Habilitar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="73"/>
      <source>Disable</source>
      <extracomment>As in: &quot;Disable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Desabilitar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="99"/>
      <source>Enable everything</source>
      <extracomment>As in &quot;Enable every single file format PhotoQt can open in any category&quot;</extracomment>
      <translation>Habilitar tudo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="117"/>
      <source>Currently there are %1 file formats enabled</source>
      <extracomment>The %1 will be replaced with the number of file formats, please don&apos;t forget to add it.</extracomment>
      <translation>Atualmente essas são %1 formatos de arquivos habilitados</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="145"/>
      <source>Search by description or file ending</source>
      <translation>Procurar por descrição ou extensão do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="151"/>
      <source>Search by image library or category</source>
      <translation>Procurar por biblioteca de imagens ou categoria</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="240"/>
      <source>File endings:</source>
      <translation>Extensão do arquivo:</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_imageview</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="31"/>
      <source>animation</source>
      <extracomment>A settings title referring to the in/out animation of images</extracomment>
      <translation>Animação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="33"/>
      <source>What type of animation to show, and how fast.</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Que tipo de animação mostrar, e quão rápido.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="44"/>
      <source>type of animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Tipo de animação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="47"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Opacidade</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="49"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Ao longo do eixo x</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="51"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Ao longo do eixo y</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="53"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">rotation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="55"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">explosion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="57"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">implosion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="59"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">choose one at random</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="76"/>
      <source>no animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Sem animação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="91"/>
      <source>long animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Animação longa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="30"/>
      <source>fit in window</source>
      <extracomment>A settings title referring to whether to fit images in window</extracomment>
      <translation>Encaixar na janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="31"/>
      <source>Zoom smaller images to fill the full window width and/or height.</source>
      <translation>Dar zoom in imagens menores para preencher a largura e/ou altura da janela.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="36"/>
      <source>fit smaller images in window</source>
      <translation>Encaixar imagens menores na janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="30"/>
      <source>interpolation</source>
      <extracomment>A settings title referring to the type of interpolation to use for small images</extracomment>
      <translation>Interpolação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="31"/>
      <source>PhotoQt tries to improve the rendering of images that are shown much larger than they are (i.e., zoomed in a lot). For very tiny images that are zoomed in quite a lot, this can result in the loss of too much information in the image. Thus a threshold can be defined here, images that are smaller than this threshold are shown exactly as they are without any smoothing or other attempts to improve them.</source>
      <translation>PhotoQt tenta aprimorar a renderização das imagens que são mostradas muito maiores do que elas são (ou seja, aproximadas um monte). Para imagens muito pequenas que são muito aproximadas, isto pode resultar em perda de muita informação na imagem. Portanto, um limite pode ser definido aqui, imagem que são menores do que este limite são mostradas exatamente como são sem qualquer suavização ou outras tentativas de melhorá-las.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="42"/>
      <source>Do not use any interpolation algorithm for very small images</source>
      <extracomment>A type of interpolation to use for small images</extracomment>
      <translation>Não use qualquer algoritmo de interpolação para imagens muito pequenas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="53"/>
      <source>threshold:</source>
      <extracomment>The threshold (in pixels) at which to switch interpolation algorithm</extracomment>
      <translation>Limite:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="30"/>
      <source>remember per session</source>
      <extracomment>A settings title</extracomment>
      <translation>Lembrar por sessão</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="31"/>
      <source>By default, PhotoQt resets the zoom, rotation, flipping/mirroring and position when switching to a different image. For certain tasks, for example for comparing two images, it can be helpful to keep these properties.</source>
      <translation>Por Padrão, o PhotoQt redefine o zoom, rotação, virar/espelhar e posição ao alterar para uma imagem diferente. Para certas tarefas, por exemplo, ao comparar duas imagens, elas podem ser úteis para manter essas propriedades.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="37"/>
      <source>remember zoom, rotation, flip, position</source>
      <translation>Lembrar zoom, rotação, virar, posição</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="30"/>
      <source>looping</source>
      <extracomment>A settings title for looping through images in folder</extracomment>
      <translation>Repetição</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="31"/>
      <source>What to do when the end of a folder has been reached: stop or loop back to first image in folder.</source>
      <translation>O que fazer quando o final de uma pastar for alcançado: parar ou voltar para primeira imagem na pasta.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="36"/>
      <source>loop through images in folder</source>
      <translation>Percorrer imagens na pasta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="30"/>
      <source>margin</source>
      <extracomment>A settings title about the margin around the main image</extracomment>
      <translation>Margem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="31"/>
      <source>How much space to show between the main image and the application border.</source>
      <translation>Quanto espaço mostrar entre a imagem principal e a borda do aplicativo.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="44"/>
      <source>none</source>
      <extracomment>As in: no margin between the main image and the window edges</extracomment>
      <translation>Nenhum</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQPixmapCache.qml" line="30"/>
      <source>pixmap cache</source>
      <extracomment>A settings title</extracomment>
      <translation>Cache pixmap</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQPixmapCache.qml" line="31"/>
      <source>Size of runtime cache for fully loaded images. This cache is cleared when the application quits.</source>
      <translation>Tamanho do cache de execução para imagens totalmente carregadas. Esse cache é limpado quando a aplicação fecha.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="31"/>
      <source>sort images by</source>
      <extracomment>A settings title</extracomment>
      <translation>Ordenar imagens por</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="32"/>
      <source>Sort all images in a folder by the set property.</source>
      <translation>Ordenar todas imagens em uma pasta pela propriedade de conjunto.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="43"/>
      <source>natural name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Nome natural</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="45"/>
      <source>name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Nome</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="47"/>
      <source>time</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Tempo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="49"/>
      <source>size</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Tamanho</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="51"/>
      <source>type</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Tipo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="58"/>
      <source>ascending</source>
      <extracomment>Sort images in ascending order</extracomment>
      <translation>Ascendente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="65"/>
      <source>descending</source>
      <extracomment>Sort images in descending order</extracomment>
      <translation>Decrescente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="30"/>
      <source>transparency marker</source>
      <extracomment>A settings title</extracomment>
      <translation>Marcador de transparência</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="31"/>
      <source>Show checkerboard pattern behind transparent areas of (half-)transparent images.</source>
      <translation>Mostrar padrão de tabuleiro de damas atrás de áreas transparentes of imagens (semi)transparentes.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="37"/>
      <source>show checkerboard pattern</source>
      <extracomment>Setting for how to display images that have transparent areas, whether to show checkerboard pattern in that area or not</extracomment>
      <translation>Mostrar padrão tabuleiro de damas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="30"/>
      <source>zoom speed</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation>Velocidade do zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="31"/>
      <source>Images are zoomed at a relative speed as specified by this percentage. A higher value means faster zoom.</source>
      <translation>Imagens são aproximadas em uma velocidade relativa como especificado por esta porcentagem. Um valor mais alto significa zoom mais rápido.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="43"/>
      <source>super slow</source>
      <extracomment>This refers to the zoom speed, the zoom here is the zoom of the main image</extracomment>
      <translation>Super lento</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="58"/>
      <source>very fast</source>
      <extracomment>This refers to the zoom speed, the zoom here is the zoom of the main image</extracomment>
      <translation>Muito rápido</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="31"/>
      <source>zoom to/from</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation type="unfinished">zoom to/from</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="32"/>
      <source>This controls whether the image is zoomed to/from the mouse position or to/from the image center. Note that this only applies when zooming by mouse, zooming by keyboard shortcut always zooms to/from the image center.</source>
      <translation type="unfinished">This controls whether the image is zoomed to/from the mouse position or to/from the image center. Note that this only applies when zooming by mouse, zooming by keyboard shortcut always zooms to/from the image center.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="42"/>
      <source>mouse position</source>
      <translation type="unfinished">mouse position</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="47"/>
      <source>image center</source>
      <translation type="unfinished">image center</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="30"/>
      <source>zoom min/max</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation type="unfinished">zoom min/max</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="31"/>
      <source>This defines the minimum and/or maximum zoom level for an image.</source>
      <translation type="unfinished">This defines the minimum and/or maximum zoom level for an image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="31"/>
      <source>Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</source>
      <translation type="unfinished">Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="52"/>
      <source>minimum zoom:</source>
      <translation type="unfinished">minimum zoom:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="91"/>
      <source>maximum zoom:</source>
      <translation type="unfinished">maximum zoom:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQDoubleClick.qml" line="30"/>
      <source>Double Click Threshold</source>
      <extracomment>A settings title</extracomment>
      <translation type="unfinished">Double Click Threshold</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQDoubleClick.qml" line="31"/>
      <source>Two clicks within the specified interval are interpreted as a double click. Note that too high a value will cause noticable delays in reacting to single clicks.</source>
      <translation type="unfinished">Two clicks within the specified interval are interpreted as a double click. Note that too high a value will cause noticable delays in reacting to single clicks.</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_interface</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="32"/>
      <source>background</source>
      <extracomment>A settings title referring to the background of PhotoQt (behind any image/element)</extracomment>
      <translation>Plano de fundo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="34"/>
      <source>What type of background is to be shown.</source>
      <extracomment>The background here refers to the area behind the main image and any element in PhotoQt, the very back.</extracomment>
      <translation>Que tipo de plano de fundo é para ser mostrado.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="45"/>
      <source>(half-)transparent background</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>Plano de fundo (semi)transparente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="47"/>
      <source>faked transparency</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>Transparência falsa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="49"/>
      <source>custom background image</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>Imagem de plano de fundo personalizada</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="76"/>
      <source>Click to select an image</source>
      <extracomment>Tooltip for a mouse area, a click on which opens a file dialog for selecting an image</extracomment>
      <translation>Clique para selecionar uma imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="87"/>
      <source>scale to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Redimensionar para encaixar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="89"/>
      <source>scale and crop to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Redimensionar e cortar para encaixar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="91"/>
      <source>stretch to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Esticar para encaixar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="93"/>
      <source>center image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Centralizar imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="95"/>
      <source>tile image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Imagem telha</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="201"/>
      <source>All Images</source>
      <extracomment>This is a category in a file dialog for selecting images used as in: All images supported by PhotoQt.</extracomment>
      <translation>Todas imagens</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="216"/>
      <source>Video</source>
      <extracomment>This is a category in a file dialog for selecting images used as in: Video files supported by PhotoQt.</extracomment>
      <translation>Vídeo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="31"/>
      <source>empty area around image</source>
      <extracomment>A settings title</extracomment>
      <translation>Área vazia em torno da imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="32"/>
      <source>How to handle clicks on empty area around images.</source>
      <translation>Como lidar com cliques sobre áreas vazias em torno de imagens.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="40"/>
      <source>close on click</source>
      <extracomment>Used as in: Close PhotoQt on click on empty area around main image</extracomment>
      <translation>Fechar com clique</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="41"/>
      <source>Close PhotoQt when click occurred on empty area around image</source>
      <translation type="unfinished">Close PhotoQt when click occurred on empty area around image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="52"/>
      <source>navigate on click</source>
      <extracomment>Used as in: Navigate in folder on click on empty area around main image</extracomment>
      <translation type="unfinished">navigate on click</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="53"/>
      <source>Go to next/previous image if click occurred in left/right half of window</source>
      <translation type="unfinished">Go to next/previous image if click occurred in left/right half of window</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="63"/>
      <source>toggle window decoration</source>
      <translation type="unfinished">toggle window decoration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="64"/>
      <source>Toggle window decoration</source>
      <translation type="unfinished">Toggle window decoration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="30"/>
      <source>size of &apos;hot edge&apos;</source>
      <extracomment>A settings title. The hot edge refers to the area along the edges of PhotoQt where the mouse cursor triggers an action (e.g., showing the thumbnails or the main menu)</extracomment>
      <translation>Tamanho de %apos;hot edge%apos;</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="31"/>
      <source>Adjusts the sensitivity of the edges for showing elements like the metadata and main menu elements.</source>
      <translation>Ajusta a sensitividade das bordas para mostrar elementos como os metadados e elementos do menu principal.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="40"/>
      <source>small</source>
      <extracomment>used as in: small area</extracomment>
      <translation>Pequeno</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="57"/>
      <source>large</source>
      <extracomment>used as in: large area</extracomment>
      <translation>Grande</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLanguage.qml" line="33"/>
      <source>language</source>
      <extracomment>A settings title.</extracomment>
      <translation>Idioma</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLanguage.qml" line="34"/>
      <source>Change the language of the application.</source>
      <translation>Mudar o idioma do aplicativo.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="30"/>
      <source>mouse wheel sensitivity</source>
      <extracomment>A settings title.</extracomment>
      <translation>Sensitividade da roda do mouse</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="31"/>
      <source>How sensitive the mouse wheel is for shortcuts, etc.</source>
      <translation>Quão sensitivo a roda do mouse é para atalhos, etc.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="58"/>
      <source>not sensitive</source>
      <extracomment>The sensitivity here refers to the sensitivity of the mouse wheel</extracomment>
      <translation>Não sensitivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="42"/>
      <source>very sensitive</source>
      <extracomment>The sensitivity here refers to the sensitivity of the mouse wheel</extracomment>
      <translation>Muito sensitivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="32"/>
      <source>overlay color</source>
      <extracomment>A settings title.</extracomment>
      <translation>Cor de sobreposição</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="33"/>
      <source>This is the color that is shown in the background on top of any background image/etc.</source>
      <translation type="unfinished">This is the color that is shown in the background on top of any background image/etc.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="77"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="148"/>
      <source>click to change color</source>
      <translation>Clique para mudar cor</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="122"/>
      <source>fullscreen mode</source>
      <translation type="unfinished">fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="186"/>
      <source>use same color in window and fullscreen mode</source>
      <translation type="unfinished">use same color in window and fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="195"/>
      <source>please choose a color</source>
      <translation>Por favor escolha a cor</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="31"/>
      <source>pop out elements</source>
      <extracomment>A settings title. The popping out that is talked about here refers to the possibility of showing any element in its own window (i.e., popped out).</extracomment>
      <translation>Destacar elementos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="32"/>
      <source>Here you can choose for most elements whether they are to be shown integrated into the main window or in their own, separate window.</source>
      <translation>Aqui você pode escolher pela maioria do elementos, se eles estão para serem mostrado integrados dentro da janela principal ou em sua própria janela separada.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="35"/>
      <source>File dialog</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Diálogo de arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="35"/>
      <source>keep open</source>
      <translation>Manter aberto</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="37"/>
      <source>Settings Manager</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Gerente de configurações</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="39"/>
      <source>Main Menu</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Menu Principal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="41"/>
      <source>Metadata</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Metadados</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="43"/>
      <source>Histogram</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Histograma</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="45"/>
      <source>Scale</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Escala</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="47"/>
      <source>Slideshow Settings</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Configurações de Apresentação de Slides</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="49"/>
      <source>Slideshow Controls</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Controles de Apresentação de Slides</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="51"/>
      <source>Rename File</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Renomear arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="53"/>
      <source>Delete File</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Deletar arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="55"/>
      <source>Save File As</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Salvar arquivo como</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="57"/>
      <source>About</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Sobre</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="59"/>
      <source>Imgur</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>imgur</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="61"/>
      <source>Wallpaper</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Papel de parede</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="63"/>
      <source>Filter</source>
      <extracomment>Noun, not a verb. Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Filtro</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="65"/>
      <source>Advanced Image Sort</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Advanced Image Sort</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="31"/>
      <source>status information</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">status information</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="32"/>
      <source>The status information shows some basic data about the current folder and photo in the top left corner of the window. The items can be reordered using drag-and-drop.</source>
      <translation type="unfinished">The status information shows some basic data about the current folder and photo in the top left corner of the window. The items can be reordered using drag-and-drop.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="41"/>
      <source>Show status information</source>
      <translation type="unfinished">Show status information</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="72"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="204"/>
      <source>counter</source>
      <extracomment>Please keep short! The counter shows where we are in the folder.</extracomment>
      <translation>Contador</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="76"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="208"/>
      <source>filepath</source>
      <extracomment>Please keep short!</extracomment>
      <translation>Caminho do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="78"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="210"/>
      <source>resolution</source>
      <extracomment>Please keep short! This is the image resolution.</extracomment>
      <translation type="unfinished">resolution</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="80"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="212"/>
      <source>zoom</source>
      <extracomment>Please keep short! This is the current zoom level.</extracomment>
      <translation type="unfinished">zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="82"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="214"/>
      <source>rotation</source>
      <extracomment>Please keep short! This is the rotation of the current image</extracomment>
      <translation type="unfinished">rotation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="84"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="216"/>
      <source>filesize</source>
      <extracomment>Please keep short! This is the filesize of the current image.</extracomment>
      <translation type="unfinished">filesize</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="231"/>
      <source>Add</source>
      <extracomment>This is written on a button that is used to add a selected block to the status info section.</extracomment>
      <translation type="unfinished">Add</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="242"/>
      <source>Font size:</source>
      <translation type="unfinished">Font size:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="74"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="206"/>
      <source>filename</source>
      <extracomment>Please keep short!</extracomment>
      <translation>Nome do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="31"/>
      <source>window buttons</source>
      <extracomment>A settings title.</extracomment>
      <translation>Botões da janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="32"/>
      <source>PhotoQt can show some integrated window buttons at the top right corner of the screen.</source>
      <translation type="unfinished">PhotoQt can show some integrated window buttons at the top right corner of the screen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="44"/>
      <source>show integrated window buttons</source>
      <translation type="unfinished">show integrated window buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="50"/>
      <source>always show &apos;x&apos;</source>
      <translation type="unfinished">always show &apos;x&apos;</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="61"/>
      <source>size of window buttons</source>
      <extracomment>the size of the window buttons (the buttons shown in the top right corner of the window)</extracomment>
      <translation>Tamanho do botões da janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="30"/>
      <source>remember last image</source>
      <extracomment>A settings title.</extracomment>
      <translation>Lembrar última imagem</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="31"/>
      <source>At startup the image loaded at the end of the last session can be automatically reloaded.</source>
      <translation>Na inicialização, a imagem carregada no fim da última sessão pode ser carregada automaticamente.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="35"/>
      <source>re-open last loaded image at startup</source>
      <translation>Reabrir última imagem carregada na inicialização</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="30"/>
      <source>tray icon</source>
      <extracomment>A settings title.</extracomment>
      <translation>Ícone da bandeja</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="31"/>
      <source>If a tray icon is to be shown and, if shown, whether to hide it or not.</source>
      <translation>Se um ícone da bandeja está para ser mostrado, e, se mostrado, seja para escondê-lo ou não.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="36"/>
      <source>no tray icon</source>
      <translation>Sem ícone da bandeja</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="37"/>
      <source>hide to tray icon</source>
      <translation>Esconder ícone da bandeja</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="38"/>
      <source>show tray icon but don&apos;t hide to it</source>
      <translation>Mostrar ícone da bandeja, porém, não%apos; escondê-lo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="31"/>
      <source>window management</source>
      <extracomment>A settings title.</extracomment>
      <translation>Gerenciamento da janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="32"/>
      <source>Some basic window management properties.</source>
      <translation>Algumas propriedades básicas de gerenciamento de janelas.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="40"/>
      <source>manage window through quick info labels</source>
      <translation>Gerenciar janela através de rótulos de inforamações rápidas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="46"/>
      <source>save and restore window geometry</source>
      <translation>Salvar e restaurar geometria da janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="51"/>
      <source>keep above other windows</source>
      <translation>Manter acima de outras janelas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="54"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="31"/>
      <source>window mode</source>
      <extracomment>A settings title.</extracomment>
      <translation>Modo janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="32"/>
      <source>Whether to run PhotoQt in window mode or fullscreen.</source>
      <translation>Seja executar PhotoQt em modo janela ou tela cheia.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="40"/>
      <source>run in window mode</source>
      <translation>Rodar em modo janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="45"/>
      <source>show window decoration</source>
      <translation>Mostrar decoração da janela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="33"/>
      <source>custom context menu entries</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">custom context menu entries</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="34"/>
      <source>Add some custom entries to the context menu.</source>
      <translation type="unfinished">Add some custom entries to the context menu.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="57"/>
      <source>Set entries for other image related applications</source>
      <translation type="unfinished">Set entries for other image related applications</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="59"/>
      <source>This will look for some other image related applications on your computer and add an entry for any that are found.</source>
      <translation type="unfinished">This will look for some other image related applications on your computer and add an entry for any that are found.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="60"/>
      <source>Note that this will replace all entries currently set and cannot be undone.</source>
      <translation type="unfinished">Note that this will replace all entries currently set and cannot be undone.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="89"/>
      <source>what string to show in main menu</source>
      <extracomment>this is the placeholder text inside of a text box telling the user what text they can enter here</extracomment>
      <translation>Que fragmento mostrar no menu principal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="113"/>
      <source>which command to execute</source>
      <extracomment>this is the placeholder text inside of a text box telling the user what text they can enter here</extracomment>
      <translation>Que comando executar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="137"/>
      <source>quit</source>
      <extracomment>Keep string short! Used on checkbox for contextmenu, refers to option to close PhotoQt after respective command has been executed.</extracomment>
      <translation>Sair</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="157"/>
      <source>move entry down</source>
      <extracomment>contextmenu settings: used as in &apos;move this entry down in the list of all entries&apos;</extracomment>
      <translation>Mover entrada para baixo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="170"/>
      <source>move entry up</source>
      <extracomment>contextmenu settings: used as in &apos;move this entry up in the list of all entries&apos;</extracomment>
      <translation>Mover entrada para cima</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="188"/>
      <source>delete entry</source>
      <extracomment>contextmenu settings: used as in &apos;delete this entry out of the list of all entries&apos;</extracomment>
      <translation>Deletar entrada</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="216"/>
      <source>Also show entries in main menu</source>
      <translation type="unfinished">Also show entries in main menu</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="31"/>
      <source>navigation buttons</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">navigation buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="32"/>
      <source>Some buttons to help with navigation. These can come in handy when, e.g., operating with a touch screen.</source>
      <translation type="unfinished">Some buttons to help with navigation. These can come in handy when, e.g., operating with a touch screen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="41"/>
      <source>buttons next to window buttons</source>
      <translation type="unfinished">buttons next to window buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="46"/>
      <source>floating buttons</source>
      <translation type="unfinished">floating buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="30"/>
      <source>popout when needed</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">popout when needed</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="31"/>
      <source>Some elements require a minimum amount of space. When the window is smaller than that, those elements can be automatically popped out to provide the space they need.</source>
      <translation type="unfinished">Some elements require a minimum amount of space. When the window is smaller than that, those elements can be automatically popped out to provide the space they need.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="36"/>
      <source>pop out when application window is small</source>
      <translation type="unfinished">pop out when application window is small</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_metadata</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="31"/>
      <source>face tags</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Etiquetas de rosto</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="33"/>
      <source>Whether to show face tags (stored in metadata info).</source>
      <extracomment>The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Seja para mostrar etiquetas de rosto (armazenadas em informações de metadados).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="38"/>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="37"/>
      <source>enable</source>
      <translation>Habilitiar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="32"/>
      <source>face tags - border</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Etiquetas de rosto - borda</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="33"/>
      <source>If and what style of border to show around tagged faces.</source>
      <translation>Se e qual estilo de borda mostrar ao redor de rostos etiquetados.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="45"/>
      <source>show border</source>
      <extracomment>The border here is the border around face tags.</extracomment>
      <translation>Mostrar borda</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="96"/>
      <source>click to change color</source>
      <translation>Clique para mudar cor</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="110"/>
      <source>please choose a color</source>
      <translation>Por favor escolha uma cor</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsFontSize.qml" line="31"/>
      <source>face tags - font size</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Etiquetas de rosto - tamanho da fonte</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsFontSize.qml" line="33"/>
      <source>The font size of the name labels.</source>
      <extracomment>The name labels here are the labels with the name used for the face tags.</extracomment>
      <translation>O tamanho da fonte dos rótulos de nome.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="31"/>
      <source>face tags - visibility</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Etiquetas de rotos - visibilidade</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="32"/>
      <source>When to show the face tags and for how long.</source>
      <translation>Quando mostrar as etiquetas de rosto e por quanto tempo.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="38"/>
      <source>hybrid mode</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>Modo híbrido</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="40"/>
      <source>always show all</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>Sempre mostrar tudo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="42"/>
      <source>show one on hover</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>Mostrar um ao passar o mouse por cima</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="44"/>
      <source>show all on hover</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>Mostrar todos ao passar o mouse por cima</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQGPSMap.qml" line="31"/>
      <source>GPS online map</source>
      <extracomment>A settings title.</extracomment>
      <translation>Mapa online de GPS</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQGPSMap.qml" line="32"/>
      <source>Which map service to use when a GPS position is clicked.</source>
      <translation>Que serviço de mapa usar quando uma posição de GPS é selecionada.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="31"/>
      <source>meta information</source>
      <extracomment>A settings title.</extracomment>
      <translation>Meta informação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="32"/>
      <source>Which meta information to extract and display.</source>
      <translation>Que meta informação extrair e exibir.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="35"/>
      <source>file name</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Nome do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="37"/>
      <source>file type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Tipo do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="39"/>
      <source>file size</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Tamanho do arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="41"/>
      <source>image #/#</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Imagem #/#</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="43"/>
      <source>dimensions</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Dimensões</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="45"/>
      <source>copyright</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Direito autoral</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="47"/>
      <source>exposure time</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Tempo de exposição</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="49"/>
      <source>flash</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Flash</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="51"/>
      <source>focal length</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Comprimento focal</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="53"/>
      <source>f-number</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Número f</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="55"/>
      <source>GPS position</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Posição do GPS</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="58"/>
      <source>keywords</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Palavras-chave</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="60"/>
      <source>light source</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Fonte de luz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="62"/>
      <source>location</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Localização</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="64"/>
      <source>make</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Criar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="66"/>
      <source>model</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Modelo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="68"/>
      <source>scene type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Tipo de cena</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="70"/>
      <source>software</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Software</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="72"/>
      <source>time photo was taken</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Foto do tempo foi tirada</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="30"/>
      <source>auto-rotation</source>
      <extracomment>A settings title.</extracomment>
      <translation>Auto-rotação</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="31"/>
      <source>Automatically rotate images based on metadata information.</source>
      <translation>Rotacionar automaticamente imagens baseados em informações de metadados.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQBehindLeftEdge.qml" line="31"/>
      <source>how to access</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">how to access</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQBehindLeftEdge.qml" line="32"/>
      <source>The meta data can be accessed in two different ways: 1) A floating element that can be moved freely, or 2) hidden behind the left screen edge and shown when the mouse cursor is moved there.</source>
      <translation type="unfinished">The meta data can be accessed in two different ways: 1) A floating element that can be moved freely, or 2) hidden behind the left screen edge and shown when the mouse cursor is moved there.</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_shortcuts</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="61"/>
      <source>quit</source>
      <extracomment>checkbox in shortcuts settings, used as in: quit PhotoQt. Please keep as short as possible!</extracomment>
      <translation>Sair</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="96"/>
      <source>Click to change shortcut</source>
      <translation>Clique para mudar atalho</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="130"/>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="144"/>
      <source>Click to delete shortcut</source>
      <translation>Clique para deletar atalho</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="89"/>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="61"/>
      <source>no shortcut set</source>
      <translation>Sem conjunto de atalho</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="71"/>
      <source>Click to manage shortcut</source>
      <translation>Clique para gerenciar atalho</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="172"/>
      <source>add new</source>
      <extracomment>Used as in &apos;add new shortcut&apos;. Please keep short!</extracomment>
      <translation>Adicionar novo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQNewShortcut.qml" line="113"/>
      <source>Add New Shortcut</source>
      <translation>Adicionar novo Atalho</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQNewShortcut.qml" line="134"/>
      <source>Perform a mouse gesture here or press any key combo</source>
      <translation>Executar a movimento de mouse aqui ou pressionar qualquer combinação de teclas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalContainer.qml" line="47"/>
      <source>Add new</source>
      <extracomment>Used on button as in &apos;add new external shortcut&apos;. Please keep short!</extracomment>
      <translation>Adicionar novo</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_thumbnails</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="30"/>
      <source>thumbnail cache</source>
      <extracomment>A settings title.</extracomment>
      <translation>cache de miniatura</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="31"/>
      <source>Thumbnails can be cached (permanently), following the freedesktop.org standard.</source>
      <translation>Miniaturas podem ser armazenadas (permanentemente), seguindo o padrão freedesktop.org.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="36"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="45"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="46"/>
      <source>enable</source>
      <translation>Habilitar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="30"/>
      <source>keep in center</source>
      <extracomment>A settings title. Used as in: Keep thumbnail for current main image in center.</extracomment>
      <translation>Manter no centro</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="31"/>
      <source>Keep currently active thumbnail in the center of the screen</source>
      <translation>Manter miniatura ativa atualmente no centro da tela</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="35"/>
      <source>center on active thumbnail</source>
      <translation>Centralizar na miniatura ativa</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="29"/>
      <source>disable thumbnails</source>
      <translation>Desabilitar miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="30"/>
      <source>Disable thumbnails in case no thumbnails are desired whatsoever.</source>
      <translation>Desabilitar miniaturas no caso de miniaturas não serem desejadas de jeito nenhum.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="35"/>
      <source>disable all thumbnails</source>
      <translation>Desabilitar todas miniaturas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="30"/>
      <source>filename label</source>
      <extracomment>A settings title. The filename label here is the one that is written on thumbnails.</extracomment>
      <translation>Rótulo de nome de arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="31"/>
      <source>Show the filename on a small label on the thumbnail image.</source>
      <translation>Mostrar o novo de arquivo em um rótulo pequeno a imagem de miniatura.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="57"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="58"/>
      <source>font size:</source>
      <translation>Tamanho da fonte:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="30"/>
      <source>filename-only</source>
      <extracomment>A settings title. This refers to using only the filename as thumbnail and no actual image.</extracomment>
      <translation>Somente nome arquivo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="31"/>
      <source>Show only the filename as thumbnail, no actual image.</source>
      <translation>Mostrar somente o nome do arquivo como miniatura, sem imagem atual.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQLiftUp.qml" line="30"/>
      <source>lift up</source>
      <extracomment>A settings title. This refers to the lift up of thumbnail images when active/hovered.</extracomment>
      <translation>Erguer</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQLiftUp.qml" line="31"/>
      <source>How many pixels to lift up thumbnails when either hovered or active.</source>
      <translation>Quantos pixel para erguer miniaturas quando, ou pairado ou ativo.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="30"/>
      <source>position</source>
      <extracomment>A settings title referring to the position of the thumbnails (upper or lower edge of PhotoQt).</extracomment>
      <translation>Posição</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="31"/>
      <source>Which edge to show the thumbnails on, upper or lower edge.</source>
      <translation>Que borda mostrar as miniaturas nas bordas superior e inferior.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="38"/>
      <source>upper edge</source>
      <extracomment>The upper edge of PhotoQt</extracomment>
      <translation>Borda superior</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="40"/>
      <source>lower edge</source>
      <extracomment>The lower edge of PhotoQt</extracomment>
      <translation>Borda inferior</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSize.qml" line="30"/>
      <source>size</source>
      <extracomment>A settings title referring to the size of the thumbnails.</extracomment>
      <translation>Tamanho</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSize.qml" line="31"/>
      <source>How large (or small) the thumbnails should be.</source>
      <translation>Quão grande (ou pequeno) as miniaturas deveriam ser.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSpacing.qml" line="30"/>
      <source>spacing</source>
      <extracomment>A settings title referring to the spacing of thumbnails, i.e., how much empty space to have between each.</extracomment>
      <translation>Espaçamento</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSpacing.qml" line="31"/>
      <source>How much space to show between the thumbnails.</source>
      <translation>Quanto espaço para mostrar entre as miniaturas.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="30"/>
      <source>threads</source>
      <extracomment>A settings title, as in: How many threads to use to generate thumbnails.</extracomment>
      <translation>Tópicos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="31"/>
      <source>How many threads to use to create thumbnails. Too many threads can slow down your computer!</source>
      <translation>Quantos tópicos usar para criar miniaturas. Muitos tópicos podem desacelerar seu computador!</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="50"/>
      <source>Threads:</source>
      <translation>Tópicos:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="30"/>
      <source>visibility</source>
      <extracomment>A settings title referring to the visibility of the thumbnails, i.e., if and when to hide them.</extracomment>
      <translation>Visibilidade</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="31"/>
      <source>If and how to keep thumbnails visible</source>
      <translation>Se e como manter miniaturas visíveis</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="37"/>
      <source>hide when not needed</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>Esconder quando não necessário</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="39"/>
      <source>never hide</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>Nunca esconder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="41"/>
      <source>hide when zoomed in</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>Esconder quando ampliado</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="29"/>
      <source>exclude folders</source>
      <translation>Excluir pastas</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="30"/>
      <source>Exclude the specified folders and all of its subfolders from any sort of caching and preloading.</source>
      <translation>Excluir as pastas específicas e todas suas subpastas de qualquer tipo de armazenamento e pré-carregamento.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="103"/>
      <source>Do not cache these folders:</source>
      <translation>Não armazenar essas pastas:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="115"/>
      <source>Add folder</source>
      <extracomment>Written on a button</extracomment>
      <translation>Adicionar pasta</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="110"/>
      <source>One folder per line</source>
      <translation>Uma pasta por linha</translation>
    </message>
  </context>
  <context>
    <name>slideshow</name>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="115"/>
      <source>Click to go to the previous image</source>
      <translation>Clique para ir para a imagem prévia</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="147"/>
      <source>Click to pause slideshow</source>
      <translation>Clique para pausar a apresentação de slides</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="148"/>
      <source>Click to play slideshow</source>
      <translation>Clique para reproduzir apresentação de slides</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="176"/>
      <source>Click to go to the next image</source>
      <translation>Clique para ir para próxima imagem</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="206"/>
      <source>Click to exit slideshow</source>
      <translation type="unfinished">Click to exit slideshow</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="256"/>
      <source>Sound volume:</source>
      <translation>Volume do som:</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="61"/>
      <location filename="../qml/slideshow/PQSlideShowSettingsPopout.qml" line="34"/>
      <source>Slideshow settings</source>
      <extracomment>Window title</extracomment>
      <translation>Configurações de apresentação de slides</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="110"/>
      <source>interval</source>
      <extracomment>The interval between images in a slideshow</extracomment>
      <translation>Intervalo</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="147"/>
      <source>animation</source>
      <extracomment>This is referring to the in/out animation of images during a slideshow</extracomment>
      <translation>Animação</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="159"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>Opacidade</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="161"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>Ao longo do eixo x</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="163"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>Ao longo do eixo y</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="165"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">rotation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="167"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">explosion</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="169"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">implosion</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="171"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">choose one at random</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="191"/>
      <source>animation speed</source>
      <extracomment>The speed of transitioning from one image to another during slideshows</extracomment>
      <translation>Velocidade de animação</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="211"/>
      <source>immediately, without animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>Imediatamente, sem animação</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="214"/>
      <source>pretty fast animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>Animação muita rápida</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="217"/>
      <source>not too fast and not too slow</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>Não muito rápido e não muito devagar</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="219"/>
      <source>very slow animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>Animação muita lenta</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="226"/>
      <source>current speed</source>
      <extracomment>This refers to the currently set speed of transitioning from one image to another during slideshows</extracomment>
      <translation>Velocidade atuaal</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="248"/>
      <source>looping</source>
      <translation>Repetição</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="261"/>
      <source>loop over all files</source>
      <extracomment>Loop over all images during slideshows</extracomment>
      <translation>Repetir todos os arquivos</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="278"/>
      <source>shuffle</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>Embaralhar</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="291"/>
      <source>shuffle all files</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>Embaralhar todos os arquivos</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="308"/>
      <source>subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>Subpastas</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="321"/>
      <source>include images in subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>Incluir imagens nas subpastas</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="339"/>
      <source>file info</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation type="unfinished">file info</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="352"/>
      <source>hide label with details about current file</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation type="unfinished">hide label with details about current file</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="370"/>
      <source>window buttons</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation type="unfinished">window buttons</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="383"/>
      <source>hide window buttons during slideshow</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation type="unfinished">hide window buttons during slideshow</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="401"/>
      <source>music</source>
      <extracomment>The music that is to be played during slideshows</extracomment>
      <translation>Música</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="418"/>
      <source>enable music</source>
      <extracomment>Enable music to be played during slideshows</extracomment>
      <translation>Habilitar música</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="425"/>
      <source>no file selected</source>
      <translation>Nenhum arquivo selecionado</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="427"/>
      <source>Click to select music file</source>
      <translation>Clique para selecionar o arquivo de música</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="428"/>
      <source>Click to change music file</source>
      <translation>Clique para mudar o arquivo de música</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="439"/>
      <source>Common music file formats</source>
      <translation>Formatos de arquivos musicais comuns</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="440"/>
      <source>All Files</source>
      <translation>Todos os arquivos</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="475"/>
      <source>Start slideshow</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Iniciar apresentação</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControlsPopout.qml" line="34"/>
      <source>Slideshow controls</source>
      <extracomment>Window title</extracomment>
      <translation>Controles de apresentação de slides</translation>
    </message>
  </context>
  <context>
    <name>startup</name>
    <message>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="323"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="325"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="327"/>
      <source>Edit with %1</source>
      <extracomment>Used as in &apos;Edit with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Editar com %1</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="329"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="331"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="333"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="335"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="337"/>
      <source>Open in %1</source>
      <extracomment>Used as in &apos;Open with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Abrir com %1</translation>
    </message>
  </context>
  <context>
    <name>streaming</name>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="98"/>
      <source>Scan for devices</source>
      <extracomment>Used as tooltip for button that starts a scan for Chromecast streaming devices in the local network</extracomment>
      <translation type="unfinished">Scan for devices</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="206"/>
      <source>Disconnect</source>
      <extracomment>Written on button, as in &apos;Disconnect from connected Chromecast streaming device&apos;</extracomment>
      <translation type="unfinished">Disconnect</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="208"/>
      <source>Connect</source>
      <extracomment>Written on button, as in &apos;Connect to Chromecast streaming device&apos;</extracomment>
      <translation type="unfinished">Connect</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="241"/>
      <source>searching for devices...</source>
      <extracomment>status text while searching for chromecast streaming devices in the local network</extracomment>
      <translation type="unfinished">searching for devices...</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="243"/>
      <source>no devices found</source>
      <extracomment>result of scan for chromecast streaming devices</extracomment>
      <translation type="unfinished">no devices found</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecastPopout.qml" line="34"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Streaming (Chromecast)</translation>
    </message>
  </context>
  <context>
    <name>thumbnailbar</name>
    <message>
      <location filename="../qml/mainwindow/PQThumbnailBar.qml" line="259"/>
      <source>File size:</source>
      <translation>Tamanho do arquivo:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQThumbnailBar.qml" line="260"/>
      <source>File type:</source>
      <translation>Tipo de arquivo:</translation>
    </message>
  </context>
  <context>
    <name>unavailable</name>
    <message>
      <location filename="../qml/unavailable/PQUnavailablePopout.qml" line="75"/>
      <source>Sorry, but this feature is not yet available on Windows.</source>
      <translation>Desculpe, mas este recurso ainda não está no Windows.</translation>
    </message>
    <message>
      <location filename="../qml/unavailable/PQUnavailablePopout.qml" line="34"/>
      <source>Feature unavailable</source>
      <extracomment>Window title, informing user that the requested feature is currently not available</extracomment>
      <translation>Recurso indisponível</translation>
    </message>
    <message>
      <location filename="../qml/unavailable/PQUnavailable.qml" line="74"/>
      <source>Sorry, but this feature is not available.</source>
      <translation type="unfinished">Sorry, but this feature is not available.</translation>
    </message>
  </context>
  <context>
    <name>wallpaper</name>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="71"/>
      <source>Warning: %1 module not activated</source>
      <translation>Aviso: %1 módulo não ativado</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="80"/>
      <location filename="../qml/wallpaper/ele/PQGnome.qml" line="69"/>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="72"/>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="81"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="69"/>
      <source>Warning: %1 not found</source>
      <translation>Aviso: %1 não encontrado</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="102"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="91"/>
      <source>Set to which screens</source>
      <extracomment>As in: Set wallpaper to which screens</extracomment>
      <translation>Configurar para quais telas</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="115"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="103"/>
      <source>Screen</source>
      <extracomment>Used in wallpaper element</extracomment>
      <translation>Tela</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="140"/>
      <source>Set to which workspaces</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Configurar para quais áreas de trabalho</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="154"/>
      <source>Workspace:</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Área de trabalho:</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQGnome.qml" line="83"/>
      <location filename="../qml/wallpaper/ele/PQWindows.qml" line="66"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="128"/>
      <source>Choose picture option</source>
      <extracomment>picture option refers to how to format a pictrue when setting it as wallpaper</extracomment>
      <translation>Escolher opção de foto</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="57"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="180"/>
      <source>Other</source>
      <extracomment>Used as in: Other Desktop Environment</extracomment>
      <translation>Outro</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="100"/>
      <source>Tool:</source>
      <extracomment>Tool refers to a program that can be executed</extracomment>
      <translation>Ferramenta:</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="106"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="124"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="142"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="160"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="178"/>
      <source>Click to choose %1</source>
      <extracomment>%1 is a placeholder for the name of a desktop environment (plasma, xfce, gnome, etc.)</extracomment>
      <translation>Clique para escolher %1</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="207"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="226"/>
      <location filename="../qml/wallpaper/PQWallpaperPopout.qml" line="34"/>
      <source>Set as Wallpaper</source>
      <extracomment>Heading of wallpaper element
----------
Written on clickable button
----------
Window title</extracomment>
      <translation>Definir como papel de parede</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQPlasma.qml" line="67"/>
      <source>The image will be set to all screens at the same time.</source>
      <translation>A imagem será definida para todas delas ao mesmo tempo.</translation>
    </message>
  </context>
</TS>
